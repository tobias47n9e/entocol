use super::schema::{collection, specimen, user};
use chrono::prelude::*;
use diesel::prelude::*;
use postgis_diesel::types::Point;
use rocket::form::{Error, Errors, FromFormField, ValueField};
use rocket::serde::{Deserialize, Serialize};
use std::borrow::Cow;

#[derive(Identifiable, Debug, Clone, Queryable, Serialize)]
#[diesel(table_name = user)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub id: i32,
    pub updated_at: DateTime<Utc>,
    pub created_at: DateTime<Utc>,
    pub username: String,
    pub email: String,

    #[serde(skip_serializing)]
    pub password: String,
}

#[derive(Insertable)]
#[diesel(table_name = user)]
pub struct NewUser<'a> {
    pub created_at: &'a NaiveDateTime,
    pub updated_at: &'a NaiveDateTime,
    pub username: &'a str,
    pub email: &'a str,
    pub password: &'a str,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct MapConfig<'a> {
    pub api_key: &'a String,
    pub map_style: &'a String,
}

#[derive(Identifiable, Associations, Debug, Serialize, Queryable)]
#[diesel(table_name = collection)]
#[diesel(belongs_to(User))]
#[serde(rename_all = "camelCase")]
pub struct Collection {
    pub id: i32,
    pub updated_at: DateTime<Utc>,
    pub created_at: DateTime<Utc>,
    pub name: String,
    pub user_id: i32,
    pub description: Option<String>,
}

#[derive(Identifiable, Associations, Debug, Serialize, Queryable)]
#[diesel(table_name = specimen)]
#[diesel(belongs_to(User))]
#[serde(rename_all = "camelCase")]
pub struct Specimen {
    pub id: i32,
    pub updated_at: DateTime<Utc>,
    pub created_at: DateTime<Utc>,
    pub user_id: i32,
    pub collected_at: DateTime<Utc>,
    pub collected_at_timezone: String,
    pub taxon: String,
    pub taxon_author: String,
    pub collected_by: String,
    pub description: String,
    pub inaturalist_id: Option<i32>,
    pub coordinates: Option<Point>,
    pub collection_id: Option<i32>,
    pub street: Option<String>,
    pub zip: Option<String>,
    pub city: Option<String>,
    pub region: Option<String>,
    pub country: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct FormTimeStamp(pub DateTime<Local>);

impl<'a> FromFormField<'a> for FormTimeStamp {
    fn from_value(field: ValueField) -> Result<FormTimeStamp, Errors<'a>> {
        match DateTime::parse_from_rfc3339(&field.value) {
            Ok(date) => Ok(FormTimeStamp(date.into())),
            Err(_) => Err(Error::validation("Invalid timestamp").into()),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct InaturalistId(pub Option<i32>);

#[derive(Debug, Deserialize)]
pub struct CollectionId(pub Option<i32>);

impl<'a> FromFormField<'a> for CollectionId {
    fn from_value(field: ValueField) -> Result<CollectionId, Errors<'a>> {
        println!("{:?}", &field.value.len());

        match &field.value.parse::<i32>() {
            Ok(id) => Ok(CollectionId((*id).into())),
            Err(_) => Err(Error::validation("Invalid collection ID").into()),
        }
    }
}

impl<'a> FromFormField<'a> for InaturalistId {
    fn from_value(field: ValueField) -> Result<InaturalistId, Errors<'a>> {
        println!("{:?}", &field.value.len());

        match &field.value.parse::<i32>() {
            Ok(id) => Ok(InaturalistId((*id).into())),
            Err(_) => Err(Error::validation("Invalid Inaturalist ID").into()),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct PostPoint {
    pub x: f64,
    pub y: f64,
}

#[derive(Deserialize)]
pub struct Register<'r> {
    pub username: &'r str,
    pub password: &'r str,
    pub password_repeat: &'r str,
    pub email: &'r str,
}

#[derive(Debug, Deserialize)]
pub struct PostPointOption(pub Option<PostPoint>);

impl<'a> FromFormField<'a> for PostPointOption {}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PostSpecimen<'r> {
    pub collected_at: FormTimeStamp,
    pub collected_at_timezone: &'r str,
    pub taxon: &'r str,
    //pub taxon_author: &'r str,
    pub collected_by: &'r str,
    pub description: &'r str,
    pub inaturalist_id: InaturalistId,
    pub coordinates: PostPointOption,
    pub collection_id: CollectionId,
    pub street: Option<&'r str>,
    pub zip: Option<&'r str>,
    pub city: Option<&'r str>,
    pub region: Option<&'r str>,
    pub country: Option<&'r str>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PostCollection<'r> {
    pub name: &'r str,
    pub description: Cow<'r, str>,
}

#[derive(Insertable)]
#[diesel(table_name = specimen)]
pub struct NewSpecimen<'a> {
    pub updated_at: &'a NaiveDateTime,
    pub created_at: &'a NaiveDateTime,
    pub user_id: &'a i32,
    pub collected_at: &'a DateTime<Local>,
    pub collected_at_timezone: &'a str,
    pub taxon: &'a str,
    pub taxon_author: &'a str,
    pub collected_by: &'a str,
    pub description: &'a str,
    pub inaturalist_id: Option<&'a i32>,
    pub coordinates: Option<&'a Point>,
    pub collection_id: Option<&'a i32>,
    pub street: Option<&'a str>,
    pub zip: Option<&'a str>,
    pub city: Option<&'a str>,
    pub region: Option<&'a str>,
    pub country: Option<&'a str>,
}

#[derive(Insertable)]
#[diesel(table_name = collection)]
pub struct NewCollection<'a> {
    pub updated_at: &'a NaiveDateTime,
    pub created_at: &'a NaiveDateTime,
    pub name: &'a str,
    pub user_id: &'a i32,
    pub description: Option<&'a str>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SpecimenAPI {
    #[serde(flatten)]
    pub specimen: Specimen,

    pub user: User,
    pub collection: Option<Collection>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct CollectionAPI {
    #[serde(flatten)]
    pub collection: Collection,

    pub user: User,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SpecimenListAPI {
    pub items: Vec<SpecimenAPI>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct CollectionListAPI {
    pub items: Vec<CollectionAPI>,
}
