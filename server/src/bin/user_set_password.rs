use clap::{command, Arg};
use entocol::database::{find_user_by_id, update_user_password};
use rpassword::read_password;
use sqlx::postgres::PgPoolOptions;
use std::env::var;
use std::io::Write;
use tokio;

#[tokio::main]
async fn main() {
    let database_url = var("DATABASE_URL").unwrap();

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await
        .unwrap();
    let matches = command!().arg(Arg::new("id").required(true)).get_matches();
    let id_string = matches.get_one::<String>("id").unwrap();
    let id = match id_string.parse::<i32>() {
        Ok(id) => id,
        Err(_) => {
            panic!("ID \"{}\" can't be parsed as a number", id_string);
        }
    };

    let user = match find_user_by_id(&id, &pool).await {
        Ok(user) => user,
        Err(_) => panic!("User with ID {} not found", &id),
    };

    println!("User found: {}", &user.username);
    print!("Type a password: ");
    std::io::stdout().flush().unwrap();
    let password = read_password().unwrap();

    if password.chars().count() < 8 {
        println!("Password should be at least 8 chars");
        return;
    }

    match update_user_password(&user, &password, &pool).await {
        Ok(user) => {
            println!("Password of user \"{}\" changed.", &user.username);
        }
        Err(_) => {
            println!("Error changing password for \"{}\".", &user.username);
        }
    };
}
