#[macro_use]
extern crate rocket;
extern crate argon2;
extern crate serde;

use entocol::models::{
    CollectionAPI, CollectionListAPI, MapConfig, PostCollection, PostSpecimen, Register,
    SpecimenAPI, SpecimenListAPI,
};
use lazy_static::lazy_static;
use rocket::fs::{relative, FileServer};
use rocket::http::{CookieJar, Status};
use rocket::outcome::IntoOutcome;
use rocket::request::{self, FromRequest, Request};
use rocket::serde::json::Json;
use rocket::State;
use rocket_jwt::jwt;
use serde::Deserialize;
use sqlx::postgres::PgPoolOptions;
use sqlx::{Pool, Postgres};
use std::env::var;
use tzf_rs::Finder;

use entocol::auth::user_password_valid;
use entocol::database::{
    create_collection_api_response, create_specimen_api_response, find_api_collection_by_id,
    find_collection_specimen, find_collections_for_user, update_collection, update_specimen,
};
use entocol::database::{
    create_user, find_api_specimen_by_id, find_api_specimen_for_user, find_user_by_username,
};

#[derive(Debug, FromForm, Deserialize)]
struct Login<'r> {
    username: &'r str,
    password: &'r str,
}

lazy_static! {
    static ref ROCKET_SECRET: String = var("ROCKET_SECRET_KEY").unwrap();
    static ref MAP_API_KEY: String = var("MAP_API_KEY").unwrap();
    static ref MAP_STYLE: String = var("MAP_STYLE").unwrap();
}

#[jwt(ROCKET_SECRET)]
pub struct UserClaim {
    id: i32,
}

#[jwt("secret", exp = 100)]
pub struct UserClaimExp {
    id: i32,
}

#[jwt("secret", leeway = 10)]
pub struct UserClaimLeeway {
    id: i32,
}

#[jwt("secret", cookie = "token")]
pub struct UserClaimCookie {
    id: i32,
}

#[jwt("secret", query = "token")]
pub struct UserClaimQuery {
    id: i32,
}

#[derive(Debug)]
struct UserId(usize);

#[rocket::async_trait]
impl<'r> FromRequest<'r> for UserId {
    type Error = std::convert::Infallible;

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<UserId, Self::Error> {
        request
            .cookies()
            .get_private("user_id")
            .and_then(|cookie| cookie.value().parse().ok())
            .map(UserId)
            .or_forward(Status::NotFound)
    }
}

#[get("/api/specimen")]
async fn api_specimen_list(
    user_claim: UserClaim,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<SpecimenListAPI>, ApiErrorResponse> {
    let specimen = find_api_specimen_for_user(&user_claim.id, &pool).await;

    Ok(Json(specimen))
}

#[get("/api/collection")]
async fn api_collection_list(
    user_claim: UserClaim,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<CollectionListAPI>, ApiErrorResponse> {
    let collections = find_collections_for_user(&user_claim.id, &pool).await;

    Ok(Json(collections))
}

#[post("/api/collection", data = "<collection>")]
async fn api_post_collection(
    user_claim: UserClaim,
    collection: Json<PostCollection<'_>>,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<CollectionAPI>, ApiErrorResponse> {
    match create_collection_api_response(&collection, &user_claim.id, &pool).await {
        Ok(collection) => Ok(Json(collection)),
        Err(_) => Err(ApiErrorResponse("Error creating collection")),
    }
}

#[put("/api/collection/<id>", data = "<collection>")]
async fn api_put_collection(
    id: i32,
    collection: Json<PostCollection<'_>>,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<CollectionAPI>, ApiErrorResponse> {
    match update_collection(&collection, &id, &pool).await {
        Ok(collection) => Ok(Json(collection)),
        Err(_) => Err(ApiErrorResponse("Error updating collection")),
    }
}

#[get("/api/specimen/<id>")]
async fn api_specimen(
    id: i32,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<SpecimenAPI>, ApiErrorResponse> {
    let specimen_option = find_api_specimen_by_id(&id, &pool);

    match specimen_option.await {
        Some(specimen) => Ok(Json(specimen)),
        None => Err(ApiErrorResponse("Error")),
    }
}

#[get("/api/collection/<id>")]
async fn api_collection(
    id: i32,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<CollectionAPI>, ApiErrorResponse> {
    match find_api_collection_by_id(&id, &pool).await {
        Some(collection) => Ok(Json(collection)),
        None => Err(ApiErrorResponse("Error")),
    }
}

#[get("/api/collection/<id>/specimen")]
async fn api_collection_specimen(
    id: i32,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<SpecimenListAPI>, ApiErrorResponse> {
    match find_collection_specimen(&id, &pool).await {
        Ok(specimen_list) => Ok(Json(specimen_list)),
        Err(error) => {
            println!("{}", error.to_string());
            Err(ApiErrorResponse("Error finding collection specimen"))
        }
    }
}

#[get("/api/profile")]
fn api_profile(user_claim: UserClaim) -> &'static str {
    println!("{:?}", user_claim);

    "test"
}

#[get("/api/timezone-finder?<lng>&<lat>")]
fn api_timezone_finder(
    _user_claim: UserClaim,
    lng: f64,
    lat: f64,
) -> Result<Json<String>, ApiErrorResponse> {
    let finder = Finder::new();

    Ok(Json(finder.get_tz_name(lng, lat).to_string()))
}

#[derive(Responder)]
#[response(status = 400, content_type = "json")]
struct ApiErrorResponse(&'static str);

#[post("/api/login", data = "<login>")]
async fn api_login(
    login: Json<Login<'_>>,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<String>, ApiErrorResponse> {
    let user_option = find_user_by_username(&login.username, &pool);

    match user_option.await {
        Ok(user) => {
            let valid = user_password_valid(&user.password, &login.password);

            if valid {
                let user_claim = UserClaim { id: user.id };
                let token = UserClaim::sign(user_claim);
                println!("{:#?}", UserClaim::decode(token.clone()));
                Ok(Json(token))
            } else {
                Err(ApiErrorResponse("Login error"))
            }
        }
        Err(_) => Err(ApiErrorResponse("Login error")),
    }
}

#[post("/api/logout")]
fn api_logout(jar: &CookieJar<'_>) -> Result<Json<&'static str>, ApiErrorResponse> {
    println!("{:?}", jar);
    // TODO: Prevent token from being reused again.
    Ok(Json("Ok"))
}

#[get("/api/map-config")]
fn api_map_config<'a>(_jar: &CookieJar<'a>) -> Result<Json<MapConfig<'a>>, ApiErrorResponse> {
    Ok(Json(MapConfig {
        api_key: &MAP_API_KEY,
        map_style: &MAP_STYLE,
    }))
}

#[post("/api/specimen", data = "<specimen>")]
async fn post_specimen(
    user_claim: UserClaim,
    specimen: Json<PostSpecimen<'_>>,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<SpecimenAPI>, ApiErrorResponse> {
    match create_specimen_api_response(&specimen, &user_claim.id, &pool).await {
        Ok(specimen) => Ok(Json(specimen)),
        Err(error) => {
            println!("{}", error.to_string());
            Err(ApiErrorResponse("Error creating specimen"))
        }
    }
}

#[put("/api/specimen/<id>", data = "<specimen>")]
async fn put_specimen(
    id: i32,
    user_claim: UserClaim,
    specimen: Json<PostSpecimen<'_>>,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<SpecimenAPI>, ApiErrorResponse> {
    match update_specimen(&specimen, &id, &user_claim.id, &pool).await {
        Ok(specimen) => Ok(Json(specimen)),
        Err(_) => Err(ApiErrorResponse("Error updating specimen")),
    }
}

#[post("/api/register", data = "<register>")]
async fn api_register(
    register: Json<Register<'_>>,
    pool: &State<Pool<Postgres>>,
) -> Result<Json<String>, ApiErrorResponse> {
    if register.password != register.password_repeat {
        return Err(ApiErrorResponse("Register error, passwords do not match"));
    }

    match create_user(&register, pool).await {
        Ok(_user) => Ok(Json("Success".to_string())),
        Err(_) => Err(ApiErrorResponse("Error")),
    }
}

#[catch(404)]
fn general_not_found() -> &'static str {
    "General 404"
}

#[launch]
async fn rocket() -> _ {
    let database_url = var("DATABASE_URL").unwrap();

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await
        .unwrap();

    rocket::build()
        .manage(pool)
        .mount(
            "/",
            routes![
                api_login,
                api_logout,
                api_map_config,
                api_profile,
                api_register,
                api_specimen,
                api_specimen_list,
                post_specimen,
                put_specimen,
                api_collection_list,
                api_post_collection,
                api_collection,
                api_put_collection,
                api_collection_specimen,
                api_timezone_finder,
            ],
        )
        .mount("/", FileServer::from(relative!("static")))
        .register("/", catchers![general_not_found])
}
