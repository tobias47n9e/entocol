use crate::models::{
    Collection, CollectionAPI, CollectionListAPI, NewSpecimen, PostCollection, PostSpecimen,
    Register, Specimen, SpecimenAPI, SpecimenListAPI, User,
};
use argon2::Config;
use chrono::prelude::*;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenvy::dotenv;
use postgis_diesel::types::Point;
use sqlx::{Pool, Postgres};
use std::env;

type Result<T, E = rocket::response::Debug<sqlx::Error>> = std::result::Result<T, E>;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub async fn create_user(
    register: &Register<'_>,
    pool: &Pool<Postgres>,
) -> Result<User, sqlx::Error> {
    let hash = hash_password(&register.password).unwrap();
    let timestamp = Utc::now();

    let user = sqlx::query_as!(
        User,
        r#"insert into "user"
        (created_at, updated_at, username, email, password)
        values ($1, $2, $3, $4, $5)
        returning id, username, created_at, email, password, updated_at"#,
        timestamp,
        timestamp,
        register.username,
        register.email,
        hash,
    )
    .fetch_one(pool)
    .await?;

    Ok(user)
}

pub async fn update_user_password(
    user_to_update: &User,
    new_password: &str,
    pool: &Pool<Postgres>,
) -> Result<User, sqlx::Error> {
    let hash = hash_password(&new_password).unwrap();
    let timestamp = Utc::now();

    let user = sqlx::query_as!(
        User,
        "update \"user\" set password = $1, updated_at = $2 where id = $3 returning id, username, created_at, email, password, updated_at",
        hash,
        timestamp,
        user_to_update.id,
    )
    .fetch_one(pool)
    .await?;

    Ok(user)
}

fn hash_password(password: &str) -> Result<std::string::String, argon2::Error> {
    let password_bytes = password.as_bytes();
    let secret_key = env::var("ROCKET_SECRET_KEY").expect("ROCKET_SECRET_KEY must be set");
    let salt = secret_key.as_bytes();
    let config = Config::default();
    argon2::hash_encoded(password_bytes, salt, &config)
}

pub fn update_specimen_db(
    put_specimen: &PostSpecimen,
    specimen_id: &i32,
    put_user_id: &i32,
) -> Result<Specimen, diesel::result::Error> {
    let connection = &mut establish_connection();

    use crate::schema::specimen::dsl::*;

    let point = match &put_specimen.coordinates.0 {
        Some(value) => Some(Point {
            x: value.x,
            y: value.y,
            srid: Some(4326),
        }),
        None => None,
    };

    diesel::update(
        specimen
            .filter(id.eq(&specimen_id))
            .filter(user_id.eq(&put_user_id)),
    )
    .set((
        taxon.eq(put_specimen.taxon),
        description.eq(put_specimen.description),
        collected_by.eq(put_specimen.collected_by),
        collection_id.eq(put_specimen.collection_id.0),
        collected_at.eq(put_specimen.collected_at.0),
        collected_at_timezone.eq(put_specimen.collected_at_timezone),
        inaturalist_id.eq(put_specimen.inaturalist_id.0.as_ref()),
        coordinates.eq(point),
        street.eq(put_specimen.street),
        zip.eq(put_specimen.zip),
        city.eq(put_specimen.city),
        region.eq(put_specimen.region),
        country.eq(put_specimen.country),
    ))
    .get_result(connection)
}

pub async fn update_collection_db(
    put_collection: &PostCollection<'_>,
    collection_id: &i32,
    pool: &Pool<Postgres>,
) -> Result<Collection, sqlx::Error> {
    let timestamp = Utc::now();

    let collection = sqlx::query_as!(
        Collection,
        "update collection set name = $1, description = $2, updated_at = $3 where id = $4 returning id, name, description, created_at, updated_at, user_id",
        &put_collection.name,
        &put_collection.description,
        timestamp,
        collection_id,
    )
        .fetch_one(pool)
        .await?;

    Ok(collection)
}

pub async fn update_specimen(
    specimen: &PostSpecimen<'_>,
    specimen_id: &i32,
    user_id: &i32,
    pool: &Pool<Postgres>,
) -> Result<SpecimenAPI, diesel::result::Error> {
    match update_specimen_db(&specimen, &specimen_id, &user_id) {
        Ok(specimen) => {
            let user = find_user_by_id(&specimen.user_id, &pool)
                .await
                .expect("Specimen should have a user");
            let collection = find_collection_by_optional_id(&specimen.collection_id, &pool)
                .await
                .expect("Collection query should work");

            Ok(SpecimenAPI {
                user,
                specimen,
                collection,
            })
        }
        Err(error) => Err(error),
    }
}

pub async fn update_collection(
    collection: &PostCollection<'_>,
    collection_id: &i32,
    pool: &Pool<Postgres>,
) -> Result<CollectionAPI, sqlx::Error> {
    match update_collection_db(&collection, &collection_id, &pool).await {
        Ok(collection) => {
            let user = find_user_by_id(&collection.user_id, &pool)
                .await
                .expect("Collection should have a user");

            Ok(CollectionAPI { user, collection })
        }
        Err(error) => Err(error),
    }
}

pub async fn create_specimen_api_response(
    specimen: &PostSpecimen<'_>,
    user_id: &i32,
    pool: &Pool<Postgres>,
) -> Result<SpecimenAPI, diesel::result::Error> {
    match create_specimen(&specimen, &user_id) {
        Ok(specimen) => {
            let user = find_user_by_id(&specimen.user_id, &pool)
                .await
                .expect("Specimen should have a user");
            let collection = find_collection_by_optional_id(&specimen.collection_id, &pool)
                .await
                .expect("Collection query should work");

            Ok(SpecimenAPI {
                user,
                specimen,
                collection,
            })
        }
        Err(error) => Err(error),
    }
}

pub async fn create_collection_api_response(
    collection: &PostCollection<'_>,
    user_id: &i32,
    pool: &Pool<Postgres>,
) -> Result<CollectionAPI, sqlx::Error> {
    match create_collection(&collection, &user_id, &pool).await {
        Ok(collection) => {
            let user = find_user_by_id(&collection.user_id, &pool)
                .await
                .expect("Collection should have a user");

            Ok(CollectionAPI { user, collection })
        }
        Err(error) => Err(error),
    }
}

pub async fn create_collection(
    collection: &PostCollection<'_>,
    user_id: &i32,
    pool: &Pool<Postgres>,
) -> Result<Collection, sqlx::Error> {
    let timestamp = Utc::now();

    let collection = sqlx::query_as!(
        Collection,
        r#"insert into collection
        (created_at, updated_at, name, user_id, description)
        values ($1, $2, $3, $4, $5)
        returning id, name, created_at, description, updated_at, user_id"#,
        timestamp,
        timestamp,
        collection.name,
        user_id,
        &collection.description,
    )
    .fetch_one(pool)
    .await?;

    Ok(collection)
}

pub fn create_specimen(
    specimen: &PostSpecimen,
    user_id: &i32,
) -> Result<Specimen, diesel::result::Error> {
    let connection = &mut establish_connection();
    let timestamp: NaiveDateTime = Utc::now().naive_utc();

    let point = match &specimen.coordinates.0 {
        Some(value) => Some(Point {
            x: value.x,
            y: value.y,
            srid: Some(4326),
        }),
        None => None,
    };

    let new_specimen = NewSpecimen {
        created_at: &timestamp,
        updated_at: &timestamp,
        collected_at: &specimen.collected_at.0,
        collected_at_timezone: &specimen.collected_at_timezone,
        collected_by: &specimen.collected_by,
        description: &specimen.description,
        taxon: &specimen.taxon,
        taxon_author: "",
        user_id: &user_id,
        inaturalist_id: specimen.inaturalist_id.0.as_ref(),
        coordinates: point.as_ref(),
        collection_id: specimen.collection_id.0.as_ref(),
        street: specimen.street,
        zip: specimen.zip,
        city: specimen.city,
        region: specimen.region,
        country: specimen.country,
    };

    use crate::schema::specimen;

    diesel::insert_into(specimen::table)
        .values(new_specimen)
        .get_result::<Specimen>(connection)
}

pub async fn find_user_by_id(user_id: &i32, pool: &Pool<Postgres>) -> Result<User, sqlx::Error> {
    let user = sqlx::query_as!(User, "select * from \"user\" where id = $1", user_id)
        .fetch_one(pool)
        .await?;

    Ok(user)
}

pub fn find_specimen_by_id(specimen_id: &i32) -> Option<Specimen> {
    use crate::schema::specimen::dsl::*;

    let connection = &mut establish_connection();
    let query = specimen.filter(id.eq(specimen_id));
    let optional = query
        .first::<Specimen>(connection)
        .optional()
        .expect("Error finding specimen");

    optional
}

pub async fn find_collection_by_id(
    collection_id: &i32,
    pool: &Pool<Postgres>,
) -> Result<Option<Collection>, sqlx::Error> {
    let collection = sqlx::query_as!(
        Collection,
        "SELECT * FROM collection c WHERE c.id = $1",
        collection_id,
    )
    .fetch_one(pool)
    .await?;

    Ok(Some(collection))
}

pub async fn find_api_specimen_by_id(
    specimen_id: &i32,
    pool: &Pool<Postgres>,
) -> Option<SpecimenAPI> {
    match find_specimen_by_id(specimen_id) {
        Some(specimen) => {
            let user = find_user_by_id(&specimen.user_id, &pool)
                .await
                .expect("Specimen should always be assigned to user");
            let collection = find_collection_by_optional_id(&specimen.collection_id, &pool)
                .await
                .expect("Collection query should work");

            Some(SpecimenAPI {
                user,
                specimen,
                collection,
            })
        }
        None => None,
    }
}

pub async fn find_api_collection_by_id(
    collection_id: &i32,
    pool: &Pool<Postgres>,
) -> Option<CollectionAPI> {
    match find_collection_by_id(collection_id, &pool).await {
        Ok(Some(collection)) => {
            let user = find_user_by_id(&collection.user_id, &pool)
                .await
                .expect("Collection should always have a user");

            Some(CollectionAPI { user, collection })
        }
        Ok(None) => None,
        Err(_) => None,
    }
}

pub async fn find_collections_for_user(user_id: &i32, pool: &Pool<Postgres>) -> CollectionListAPI {
    let user = find_user_by_id(&user_id, &pool)
        .await
        .expect("Error finding user");
    let collections: Vec<Collection> = sqlx::query_as!(
        Collection,
        "SELECT * FROM collection c WHERE c.user_id = $1",
        user_id
    )
    .fetch_all(pool)
    .await
    .expect("Error fetching collections");

    let mut mapped_items = vec![];

    for item in collections {
        mapped_items.push(CollectionAPI {
            collection: item,
            user: user.clone(),
        });
    }

    CollectionListAPI {
        items: mapped_items,
    }
}

async fn find_collection_by_optional_id(
    id: &Option<i32>,
    pool: &Pool<Postgres>,
) -> Result<Option<Collection>, sqlx::Error> {
    match id {
        Some(id) => find_collection_by_id(&id, &pool).await,
        None => Ok(None),
    }
}

pub async fn find_collection_specimen(
    collection_id_input: &i32,
    pool: &Pool<Postgres>,
) -> Result<SpecimenListAPI, diesel::result::Error> {
    use crate::schema::specimen::dsl::*;

    let connection = &mut establish_connection();
    let query = specimen.filter(collection_id.eq(collection_id_input));
    let items = query.load::<Specimen>(connection)?;
    let mut mapped_items = vec![];

    for item in items {
        let user = find_user_by_id(&item.user_id, &pool)
            .await
            .expect("Specimen should always be assigned to user");
        let collection = find_collection_by_optional_id(&item.collection_id, &pool)
            .await
            .expect("Collection query should work");

        mapped_items.push(SpecimenAPI {
            user,
            specimen: item,
            collection,
        });
    }

    Ok(SpecimenListAPI {
        items: mapped_items,
    })
}

pub async fn find_api_specimen_for_user(
    user_id_filter: &i32,
    pool: &Pool<Postgres>,
) -> SpecimenListAPI {
    use crate::schema::specimen::dsl::*;

    let connection = &mut establish_connection();
    let query = specimen.filter(user_id.eq(user_id_filter));
    let items = query
        .load::<Specimen>(connection)
        .expect("Error finding specimen");
    let mut mapped_items = vec![];

    for item in items {
        let user = find_user_by_id(&item.user_id, &pool)
            .await
            .expect("Specimen should always be assigned to user");
        let collection = find_collection_by_optional_id(&item.collection_id, &pool)
            .await
            .expect("Collection query should work");

        mapped_items.push(SpecimenAPI {
            user,
            specimen: item,
            collection,
        });
    }

    SpecimenListAPI {
        items: mapped_items,
    }
}

pub async fn find_user_by_username(
    username: &str,
    pool: &Pool<Postgres>,
) -> Result<User, sqlx::Error> {
    let user = sqlx::query_as!(
        User,
        "SELECT * FROM \"user\" u WHERE u.username = $1",
        username
    )
    .fetch_one(pool)
    .await?;

    Ok(user)
}
