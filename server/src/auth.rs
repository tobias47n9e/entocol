pub fn user_password_valid(hash: &str, password: &str) -> bool {
    argon2::verify_encoded(&hash, password.as_bytes()).unwrap()
}
