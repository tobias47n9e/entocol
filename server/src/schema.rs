// @generated automatically by Diesel CLI.

diesel::table! {
    collection (id) {
        id -> Int4,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        name -> Varchar,
        user_id -> Int4,
        description -> Nullable<Text>,
    }
}

diesel::table! {
    spatial_ref_sys (srid) {
        srid -> Int4,
        auth_name -> Nullable<Varchar>,
        auth_srid -> Nullable<Int4>,
        srtext -> Nullable<Varchar>,
        proj4text -> Nullable<Varchar>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use postgis_diesel::sql_types::Geometry;

    specimen (id) {
        id -> Int4,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        user_id -> Int4,
        collected_at -> Timestamptz,
        collected_at_timezone -> Varchar,
        taxon -> Varchar,
        taxon_author -> Varchar,
        collected_by -> Varchar,
        description -> Text,
        inaturalist_id -> Nullable<Int4>,
        coordinates -> Nullable<Geometry>,
        collection_id -> Nullable<Int4>,
        street -> Nullable<Varchar>,
        city -> Nullable<Varchar>,
        zip -> Nullable<Varchar>,
        region -> Nullable<Varchar>,
        country -> Nullable<Varchar>,
    }
}

diesel::table! {
    user (id) {
        id -> Int4,
        updated_at -> Timestamptz,
        created_at -> Timestamptz,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
    }
}

diesel::joinable!(collection -> user (user_id));
diesel::joinable!(specimen -> user (user_id));

diesel::allow_tables_to_appear_in_same_query!(collection, spatial_ref_sys, specimen, user,);
