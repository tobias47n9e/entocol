ALTER TABLE "specimen" ADD COLUMN collected_at timestamptz NULL;
ALTER TABLE "specimen" ADD COLUMN taxon varchar NULL;
ALTER TABLE "specimen" ADD COLUMN taxon_author varchar NULL;
ALTER TABLE "specimen" ADD COLUMN collected_by varchar NULL;
ALTER TABLE "specimen" ADD COLUMN description text NULL;
