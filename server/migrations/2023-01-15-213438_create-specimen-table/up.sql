CREATE TABLE specimen (
     id serial4 NOT NULL,
     created_at timestamptz NOT NULL,
     updated_at timestamptz NOT NULL,
     user_id int4 NOT NULL,
     CONSTRAINT specimen_pk PRIMARY KEY (id),
     CONSTRAINT specimen_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id)
);
