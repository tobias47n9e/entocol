-- Your SQL goes here

CREATE TABLE collection (
    id serial4 NOT NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    "name" varchar NOT NULL,
    user_id int4 NOT NULL,
    description text NULL,
    CONSTRAINT collection_pk PRIMARY KEY (id),
    CONSTRAINT collection_user_fk FOREIGN KEY (user_id) REFERENCES "user" (id)
);

ALTER TABLE specimen ADD collection_id int4 NULL;
