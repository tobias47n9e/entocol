CREATE TABLE "user" (
    id serial NOT NULL,
    updated_at timestamptz NOT NULL,
    created_at timestamptz NOT NULL,
    username varchar NOT NULL,
    email varchar NOT NULL,
    "password" varchar NOT NULL,
    CONSTRAINT user_id_pk PRIMARY KEY (id)
);
