CREATE TABLE "user"
(
    id         serial4     NOT NULL,
    updated_at timestamptz NOT NULL,
    created_at timestamptz NOT NULL,
    username   varchar     NOT NULL,
    email      varchar     NOT NULL,
    "password" varchar     NOT NULL,
    CONSTRAINT email_unique UNIQUE (email),
    CONSTRAINT user_id_pk PRIMARY KEY (id),
    CONSTRAINT username_unique UNIQUE (username)
);

CREATE TABLE taxon
(
    id                    serial4 NOT NULL,
    parent_inaturalist_id int4 NULL,
    "rank"                varchar NOT NULL,
    "name"                varchar NOT NULL,
    inaturalist_id        int4    NOT NULL,
    CONSTRAINT taxon_pk PRIMARY KEY (id)
);
CREATE UNIQUE INDEX taxon_inaturalist_id_idx ON taxon USING btree (inaturalist_id);
CREATE INDEX taxon_parent_inaturalist_id_idx ON taxon USING btree (parent_inaturalist_id);

CREATE TABLE collection
(
    id          serial4     NOT NULL,
    created_at  timestamptz NOT NULL,
    updated_at  timestamptz NOT NULL,
    "name"      varchar     NOT NULL,
    user_id     int4        NOT NULL,
    description text NULL,
    CONSTRAINT collection_pk PRIMARY KEY (id)
);

ALTER TABLE collection
    ADD CONSTRAINT collection_user_fk FOREIGN KEY (user_id) REFERENCES "user" (id);

CREATE TABLE specimen
(
    id                    serial4     NOT NULL,
    created_at            timestamptz NOT NULL,
    updated_at            timestamptz NOT NULL,
    user_id               int4        NOT NULL,
    collected_at          timestamptz NOT NULL,
    "label"               varchar     NOT NULL,
    taxon_author          varchar     NOT NULL,
    collected_by          varchar     NOT NULL,
    description           text        NOT NULL,
    inaturalist_id        int4 NULL,
    collection_id         int4 NULL,
    street                varchar NULL,
    city                  varchar NULL,
    zip                   varchar NULL,
    region                varchar NULL,
    country               varchar NULL,
    collected_at_timezone varchar NULL,
    taxon_id              int4 NULL,
    coordinates public.geometry NULL,
    CONSTRAINT specimen_pk PRIMARY KEY (id)
);

ALTER TABLE public.specimen
    ADD CONSTRAINT specimen_taxon_fk FOREIGN KEY (taxon_id) REFERENCES taxon (id);
ALTER TABLE public.specimen
    ADD CONSTRAINT specimen_user_fk FOREIGN KEY (user_id) REFERENCES "user" (id);
