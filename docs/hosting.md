# Hosting

To host the application you can use a `docker-compose.yml` file on the server that
uses the server and client container. You can start out with from this example:

```yaml
version: "3.8"

services:
    proxy:
        image: nginx
        restart: always
        ports:
            - 12345:80 # Replace the port with your incoming port
        volumes:
            - /etc/localtime:/etc/localtime:ro
            - ./nginx.conf:/etc/nginx/nginx.conf:ro
        depends_on:
            - server
            - client
            - postgres
    server:
        image: server-image # Replace with the current server image tag
        restart: always
        tty: true
        ports:
          - 20007:80
        environment:
          ROCKET_SECRET_KEY: "SECRET" # Replace with a valid secret
          ROCKET_ADDRESS: "0.0.0.0"
          ROCKET_PORT: 80
          DATABASE_URL: postgres://user:pass@postgres:5432/entocol
          POSTGRES_USER: user
          POSTGRES_PASSWORD: pass
          POSTGRES_DB: entocol
        depends_on:
            - postgres
        command:
          - bash
          - -c
          - |
            diesel migration run
            ./entocol
    client:
        image: client-image # Replace with current client image tag
        working_dir: /app
        ports:
          - 20006:80
        tty: true
    postgres:
        image: postgis/postgis:15-3.3
        restart: always
        environment:
          POSTGRES_USER: user
          POSTGRES_PASSWORD: pass
          POSTGRES_DB: entocol
        expose:
          - 5432
        ports:
            - "25433:5432"
        volumes:
            - /etc/localtime:/etc/localtime:ro
            - pg_transfer_prod:/transfer
            - pg_data_prod:/var/lib/postgresql/data
volumes:
  pg_data_prod:
  pg_transfer_prod:
```

## Proxy

The proxy container can use this config to split the traffic between `https://<HOST>` and
`https://<HOST>/api`:

```nginx configuration
events {
    worker_connections 1024;
}

http {
    include /etc/nginx/mime.types;
    client_max_body_size 100M;

    server {
        listen 80;

        location /api {
            proxy_pass http://server:80;
        }

        location / {
            proxy_pass http://client:80;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_http_version 1.1;
            proxy_cache_bypass $http_upgrade;
        }
    }
}
```
