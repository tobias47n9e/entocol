# Database

This command can be used to dump the database:

```
pg_dump -F c -f /transfer/backup.tar.gz -U <USER> <DB>
```

This command can be used to restore the database:

```
pg_restore -c -U <USER> -d <BV> -v /transfer/backup.tar.gz -W
```
