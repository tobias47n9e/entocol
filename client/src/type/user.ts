import type { Timestamp } from './timestamp';

export interface User {
  id: number;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  username: string;
  email: string;
}
