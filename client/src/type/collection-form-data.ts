export interface CollectionFormData {
  id: number | null;
  name: string | null;
  description: string | null;
}
