export interface Coordinates {
  x: number;
  y: number;
  srid?: number | null;
}
