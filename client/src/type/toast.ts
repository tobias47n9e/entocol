export type Toast = {
  id: number;
  type: 'info' | 'error' | 'success';
  dismissible: boolean;
  timeout: number | null;
  message: string;
};
