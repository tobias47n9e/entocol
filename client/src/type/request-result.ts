import type { RequestState } from './request-state';

export type SuccessResult<T> = {
  state: RequestState.Ok;
  response: T;
};

export type ErrorResult = {
  state: RequestState.Error;
};

export type RequestResult<T> = SuccessResult<T> | ErrorResult;
