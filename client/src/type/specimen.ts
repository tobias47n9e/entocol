import type { User } from './user';
import type { FormSpecimen } from './form-specimen';

export interface Specimen extends FormSpecimen {
  id: number;
  userId: number;
  user: User;
}
