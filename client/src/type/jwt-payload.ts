export interface JwtPayload {
  exp: number;
  iat: number;
  user: {
    id: number;
  };
}
