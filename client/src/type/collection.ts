import type { Timestamp } from './timestamp';
import type { User } from './user';

export interface Collection {
  id: number;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  name: string;
  description: string;
  user: User;
}
