export enum DetailPageState {
  Loading = 'Loading',
  Ready = 'Ready',
  Error = 'Error',
}
