export type MapConfig = {
  apiKey: string;
  mapStyle: string;
};
