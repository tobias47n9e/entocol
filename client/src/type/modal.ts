export enum Modal {
  ChooseLocation = 'choose_location',
  GenerateDocument = 'generate_document',
}
