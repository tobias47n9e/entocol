import type { Timestamp } from './timestamp';
import type { Coordinates } from './coordinates';
import type { Collection } from './collection';

export interface FormSpecimen {
  collectedAt: Timestamp;
  collectedAtTimezone: string;
  taxon: string;
  taxonAuthor: string;
  collectedBy: string;
  description: string;
  collection?: Collection | null;
  inaturalistId: number | null;
  collectionId: number | null;
  coordinates: Coordinates | null;
  street: string | null;
  zip: string | null;
  city: string | null;
  region: string | null;
  country: string | null;
}
