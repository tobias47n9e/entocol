import type { Toast } from '../../type/toast';
import type { PartialBy } from '../../type/utilityTypes';
import { writable } from 'svelte/store';

export const toasts = writable<Toast[]>([]);

export const toast = (toast: Omit<PartialBy<Toast, 'dismissible' | 'timeout'>, 'id'>): void => {
  const id = Math.floor(Math.random() * 10000);

  const defaults = {
    id,
    dismissible: true,
    timeout: 3000,
  };

  const toastObj = { ...defaults, ...toast };

  toasts.update((toasts) => [toastObj, ...toasts]);

  if (toastObj.timeout) setTimeout(() => dismissToast(id), toastObj.timeout);
};

export const dismissToast = (id: number) => {
  toasts.update((toasts) => toasts.filter((toast) => toast.id !== id));
};
