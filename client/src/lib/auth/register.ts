import { goto } from '$app/navigation';
import { toast } from '$lib/stores/toasts';
import { apiPost } from '../request';
import { RequestState } from '../../type/request-state';

export const register = async (
  username: string,
  password: string,
  password_repeat: string,
  email: string
) => {
  const result = await apiPost('/api/register', {
    username,
    password,
    password_repeat,
    email,
  });

  if (result.state === RequestState.Ok) {
    return goto('/login');
  }

  toast({ message: 'Invalid credentials', type: 'error' });
};
