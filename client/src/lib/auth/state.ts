import { writable } from 'svelte/store';
import Cookies from 'js-cookie';
import type { Modal } from '../../type/modal';

function userLoggedIn(): boolean {
  return !!Cookies.get('jwt');
}

export const loggedInWritable = writable<boolean>(userLoggedIn());
export const modal = writable<Modal | null>(null);
