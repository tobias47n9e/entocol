import { goto } from '$app/navigation';
import axios, {
  type AxiosHeaders,
  type AxiosResponse,
  type AxiosError,
  type InternalAxiosRequestConfig,
} from 'axios';
import Cookies from 'js-cookie';

export const setupInterceptors = () => {
  axios.interceptors.request.use(
    (config: InternalAxiosRequestConfig): InternalAxiosRequestConfig => {
      config.headers = { ...config.headers } as AxiosHeaders;
      const jwt = Cookies.get('jwt');
      if (jwt) {
        (config.headers as any).Authorization = `Bearer ${jwt}`;
      } else {
        console.log('No token');
      }

      return config;
    }
  );

  axios.interceptors.response.use(
    (response: AxiosResponse) => response,
    (error: AxiosError) => {
      if (error.response?.status === 401) {
        console.log('User is unauthorized - Redirecting to /login');
        goto('/login');
      }
      return Promise.reject(error);
    }
  );
};
