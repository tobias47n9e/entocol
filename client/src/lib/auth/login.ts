import Cookies from 'js-cookie';
import { goto } from '$app/navigation';
import { loggedInWritable } from '$lib/auth/state';
import { toast } from '$lib/stores/toasts';
import { apiPost } from '../request';
import { RequestState } from '../../type/request-state';

export const login = async (username: string, password: string) => {
  const result = await apiPost<string>('/api/login', {
    username,
    password,
  });

  if (result.state === RequestState.Error) {
    toast({ message: 'Invalid credentials', type: 'error' });
    return;
  }

  Cookies.set('jwt', result.response);
  loggedInWritable.set(true);
  toast({ message: 'Logged in', type: 'success' });

  // Redirect to /profile on successful sign in. TODO: Maybe we want to redirect somewhere else?
  return goto('/profile');
};
