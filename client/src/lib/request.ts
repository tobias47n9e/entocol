import { RequestState } from '../type/request-state';
import type { RequestResult } from '../type/request-result';
import axios from 'axios';

export function apiGet<T>(path: string): Promise<RequestResult<T>> {
  return new Promise((resolve) => {
    axios
      .get<T>(path)
      .then((response) => {
        return resolve({ state: RequestState.Ok, response: response.data });
      })
      .catch(() => {
        return resolve({ state: RequestState.Error });
      });
  });
}

export function apiPut<T>(path: string, data: object): Promise<RequestResult<T>> {
  return new Promise((resolve) => {
    axios
      .put<T>(path, data)
      .then((response) => {
        return resolve({ state: RequestState.Ok, response: response.data });
      })
      .catch(() => {
        return resolve({ state: RequestState.Error });
      });
  });
}

export function apiPost<T>(path: string, data: object): Promise<RequestResult<T>> {
  return new Promise((resolve) => {
    axios
      .post<T>(path, data)
      .then((response) => {
        return resolve({ state: RequestState.Ok, response: response.data });
      })
      .catch(() => {
        return resolve({ state: RequestState.Error });
      });
  });
}
