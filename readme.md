# Entocol

Very early prototype for an entomology collection application.

## Install

The database and Rocket run using Docker. You can start the containers using:

```
docker-compose up -d
```

Inside the `server` container the migrations can be run:

```
diesel migration run
```

Insert some required ENV variables into a `.env` file in the `server` directory:

```
DATABASE_URL=postgres://user:pass@postgres:5432/entocol
ROCKET_SECRET_KEY=<SECRET_KEY>
```

The `server` container is running cargo watch, which recompiles when changes are made. You can follow the logs
and see if the changes compile using:

```
docker-compose logs -f server
```

## Debug database connection problems

If you can a connection error with the database, you might need to change
the ownership of the files inside the database container:

```
chown -R postgres:postgres var/lib/postgresql/
```

To check the connection you can also install the package `postgresql` in the
`server` container and connect to the database like this:

```
psql -h postgres -p 5432 -U user -d entocol -W
```
